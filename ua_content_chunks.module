<?php
/**
 * @file
 * Code for the UA Content Chunks feature.
 */

include_once 'ua_content_chunks.features.inc';
include_once 'ua_content_chunks.theme_field_overrides.inc';
include_once 'ua_content_chunks.preprocess_entity_overrides.inc';

/**
 * Override or insert variables into the UA Content Chunks templates.
 *
 * Based on the optional extra function
 * STARTERKIT_preprocess_node(&$variables, $hook) from template.php in the Zen
 * theme,
 * http://drupal.stackexchange.com/questions/11114/is-it-possible-to-define-a-hook-preprocess-function-based-on-entity-type-such-as
 * and https://www.drupal.org/node/2521802
 *
 * @param array &$variables
 *   An array of variables to pass to the theme template.
 * @param string $hook
 *   The hook name.
 */
function ua_content_chunks_preprocess_entity(array &$variables, $hook) {
  $entity_type = $variables['elements']['#entity_type'];
  if ($entity_type == 'paragraphs_item') {
    $bundle = $variables['elements']['#bundle'];
    $function = __FUNCTION__ . '_paragraphs_item_' . $bundle;
    if (function_exists($function)) {
      $function($variables, $hook);
    }
  }
}

/**
 * Implements hook_theme_registry_alter().
 *
 * Taken directly from https://www.drupal.org/node/715160
 * (Using template (.tpl.php) files in your own module).
function ua_content_chunks_theme_registry_alter(&$theme_registry) {
  // Defined path to the current module.
  $module_path = drupal_get_path('module', 'ua_content_chunks');
  // Find all .tpl.php files in this module's folder recursively.
  $template_file_objects = drupal_find_theme_templates($theme_registry, '.tpl.php', $module_path);
  // Iterate through all found template file objects.
  foreach ($template_file_objects as $key => $template_file_object) {
    // If the template has not already been overridden by a theme.
    if (!isset($theme_registry[$key]['theme path']) || !preg_match('#/themes/#', $theme_registry[$key]['theme path'])) {
      // Alter the theme path and template elements.
      $theme_registry[$key]['theme path'] = $module_path;
      $theme_registry[$key] = array_merge($theme_registry[$key], $template_file_object);
      $theme_registry[$key]['type'] = 'module';
    }
  }
}
 */

/**
 * Implements hook_theme().
 */
function ua_content_chunks_theme() {
  $ourname = 'ua_content_chunks';
  $ourprefix = 'paragraphs_item__';
  $fileprefix = strtr($ourprefix, '_', '-');
  $content_chunk_types = array(
    'ua_column_image',
    'ua_file_download',
    'ua_headed_text',
    'ua_plain_text',
  );
  $theme_dir_path = drupal_get_path('module', $ourname) . '/templates';
  $theme = array();
  foreach ($content_chunk_types as $cctype) {
    $theme[$ourprefix . $cctype] = array(
      'render element' => 'elements',
      'template' => ($fileprefix . $cctype),
      'path' => $theme_dir_path,
    );
  }
  $host_item_group = 'paragraphs_items';
  $theme[$host_item_group] = array(
    'render element' => 'element',
    'template' => strtr($host_item_group, '_', '-'),
    'path' => $theme_dir_path,
  );
  return $theme;
}
